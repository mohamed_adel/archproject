library ieee ;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
entity ControlUnit is

port(	IR:	in std_logic_vector(15 downto 0);
	Flags:	in std_logic_vector(15 downto 0);
	CLK:in std_logic);
end ControlUnit;
