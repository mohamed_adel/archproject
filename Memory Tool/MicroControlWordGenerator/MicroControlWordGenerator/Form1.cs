﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace MicroControlWordGenerator {
    public partial class Form1 : Form {
        public Form1() {
            InitializeComponent();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e) {

        }
        Dictionary<string, string> F1 = new Dictionary<string, string>();
        Dictionary<string, string> F2 = new Dictionary<string, string>();
        Dictionary<string, string> F3 = new Dictionary<string, string>();
        Dictionary<string, string> F4 = new Dictionary<string, string>();
        Dictionary<string, string> F5 = new Dictionary<string, string>();
        Dictionary<string, string> F6 = new Dictionary<string, string>();
        Dictionary<string, string> F7 = new Dictionary<string, string>();
        Dictionary<string, string> F8 = new Dictionary<string, string>();
        Dictionary<string, string> F9 = new Dictionary<string, string>();
        Dictionary<string, string> F10 = new Dictionary<string, string>();
        Dictionary<string, string> F11 = new Dictionary<string, string>();

        List<KeyValuePair<int, string>> memItems = new List<KeyValuePair<int, string>>();

        private void Form1_Load(object sender, EventArgs e) {

            F1["0000"] = "No transfer";
            F1["0001"] = "PC out";
            F1["0010"] = "MDR out";
            F1["0011"] = "Z out";
            F1["0100"] = "Rsrc out";
            F1["0101"] = "Rdest out";
            F1["1000"] = "Source out";
            F1["1001"] = "Destin out";
            F1["1010"] = "Temp out";

            F2["000"] = "No transfer";
            F2["001"] = "PC in";
            F2["010"] = "IR in";
            F2["011"] = "Z in";
            F2["100"] = "Rsrc in";
            F2["101"] = "Rdst in";

            F3["00"] = "No transfer";
            F3["01"] = "MAR IN";
            F3["10"] = "MDR IN";
            F3["11"] = "TEMP IN";

            F4["00"] = "No transfer";
            F4["01"] = "Y IN";
            F4["10"] = "source IN";
            F4["11"] = "dest IN";

            F5["00000"] = "Inc";
            F5["00001"] = "Add";
            F5["00010"] = "Sub";
            F5["00011"] = "Dec";
            F5["00100"] = "Bic";
            F5["00101"] = "Bit";
            F5["00110"] = "Bis";
            F5["00111"] = "Xor";
            F5["01000"] = "LSR";
            F5["01001"] = "ROR";
            F5["01010"] = "RORC";
            F5["01011"] = "ASR";
            F5["01100"] = "LSL";
            F5["01101"] = "ROL";
            F5["01110"] = "ROLC";
            F5["01111"] = "ASL";
            F5["10000"] = "Mov";
            F5["10001"] = "CMP";
            F5["10010"] = "CLR";
            F5["10011"] = "INV";


            F6["00"] = "No transfer";
            F6["01"] = "READ";
            F6["10"] = "WRITE";

            F7["0"] = "No ACTION";
            F7["1"] = "CLEAR Y";


            F8["0"] = "carry=0";
            F8["1"] = "CARRY=1";

            F9["0"] = "No ACTION";
            F9["1"] = "WMFC";

            F10["0"] = "No flags effect";
            F10["1"] = "Flag in";

            F11["0"] = "NO action";
            F11["1"] = "PLA";


            listBox1.DataSource = new BindingSource(F1, null);
            listBox1.DisplayMember = "Value";
            listBox1.ValueMember = "Key";

            listBox2.DataSource = new BindingSource(F2, null);
            listBox2.DisplayMember = "Value";
            listBox2.ValueMember = "Key";

            listBox3.DataSource = new BindingSource(F3, null);
            listBox3.DisplayMember = "Value";
            listBox3.ValueMember = "Key";

            listBox4.DataSource = new BindingSource(F4, null);
            listBox4.DisplayMember = "Value";
            listBox4.ValueMember = "Key";

            listBox5.DataSource = new BindingSource(F5, null);
            listBox5.DisplayMember = "Value";
            listBox5.ValueMember = "Key";

            listBox6.DataSource = new BindingSource(F6, null);
            listBox6.DisplayMember = "Value";
            listBox6.ValueMember = "Key";

            listBox7.DataSource = new BindingSource(F7, null);
            listBox7.DisplayMember = "Value";
            listBox7.ValueMember = "Key";

            listBox8.DataSource = new BindingSource(F8, null);
            listBox8.DisplayMember = "Value";
            listBox8.ValueMember = "Key";

            listBox9.DataSource = new BindingSource(F9, null);
            listBox9.DisplayMember = "Value";
            listBox9.ValueMember = "Key";

            listBox10.DataSource = new BindingSource(F10, null);
            listBox10.DisplayMember = "Value";
            listBox10.ValueMember = "Key";

            listBox11.DataSource = new BindingSource(F11, null);
            listBox11.DisplayMember = "Value";
            listBox11.ValueMember = "Key";

            checkedListBox1.SetItemChecked(0, true);

        }

        private void button1_Click(object sender, EventArgs e) {
            if (textBox1.Text == string.Empty || textBoxCurrentAddress.Text == string.Empty) {
                MessageBox.Show("Enter Next and Current Address!");
                return;
            }

            string binary;
            binary = Convert.ToString(Convert.ToInt32(textBox1.Text, 8), 2);
            string outs = binary;
            while (outs.Length < 12) outs = '0' + outs;
            KeyValuePair<string, string> k1 = (System.Collections.Generic.KeyValuePair<string, string>)listBox1.SelectedItem;
            KeyValuePair<string, string> k2 = (System.Collections.Generic.KeyValuePair<string, string>)listBox2.SelectedItem;
            KeyValuePair<string, string> k3 = (System.Collections.Generic.KeyValuePair<string, string>)listBox3.SelectedItem;
            KeyValuePair<string, string> k4 = (System.Collections.Generic.KeyValuePair<string, string>)listBox4.SelectedItem;
            KeyValuePair<string, string> k5 = (System.Collections.Generic.KeyValuePair<string, string>)listBox5.SelectedItem;
            KeyValuePair<string, string> k6 = (System.Collections.Generic.KeyValuePair<string, string>)listBox6.SelectedItem;
            KeyValuePair<string, string> k7 = (System.Collections.Generic.KeyValuePair<string, string>)listBox7.SelectedItem;
            KeyValuePair<string, string> k8 = (System.Collections.Generic.KeyValuePair<string, string>)listBox8.SelectedItem;
            KeyValuePair<string, string> k9 = (System.Collections.Generic.KeyValuePair<string, string>)listBox9.SelectedItem;
            KeyValuePair<string, string> k10 = (System.Collections.Generic.KeyValuePair<string, string>)listBox10.SelectedItem;
            KeyValuePair<string, string> k11 = (System.Collections.Generic.KeyValuePair<string, string>)listBox11.SelectedItem;
            string y = outs + k1.Key + k2.Key + k3.Key + k4.Key + k5.Key + k6.Key + k7.Key + k8.Key + k9.Key + k10.Key + k11.Key;
            textBox2.Text = Convert.ToString(Convert.ToInt32(y, 2), 16);

            memItems.Add(new KeyValuePair<int,string>(
                Convert.ToInt32(Convert.ToString(Convert.ToInt32(textBoxCurrentAddress.Text, 8), 10)),
                textBox2.Text));
            label3.Text = Convert.ToString(memItems.Count);
        }

        private void checkedListBox1_SelectedIndexChanged(object sender, EventArgs e) {

        }

        private void checkedListBox1_ItemCheck(object sender, ItemCheckEventArgs e) {
            if (e.NewValue == CheckState.Checked)
                for (int ix = 0; ix < checkedListBox1.Items.Count; ++ix)
                    if (e.Index != ix) checkedListBox1.SetItemChecked(ix, false);
        }

        private void buttonSaveToFile_Click(object sender, EventArgs e) {
            var FD = new OpenFileDialog();
            FD.Filter = "Memory Files|*.mem";
            FD.Title = "Select Memroy File";

            if (FD.ShowDialog() == DialogResult.OK) {
                string fileToOpen = FD.FileName;
                
                var text = File.ReadLines(fileToOpen).Select(s => s.Split(' ')).ToList();

                System.IO.StreamWriter changes = new System.IO.StreamWriter(Environment.CurrentDirectory
                    + "/Changes.txt");
                changes.WriteLine("Decimal - Octal - Control Signal");
                foreach (var it in memItems) {
                    var i = 3 + it.Key/10;
                    var j = text[i].Length - 10 + it.Key % 10;
                    text[i][j] = new string('0', 9 - it.Value.Length) + it.Value;
                    changes.WriteLine(it.Key + " - " + Convert.ToString(it.Key, 8) + " - " + it.Value);
                }
                changes.Close();
                memItems.Clear();
                label3.Text = Convert.ToString(memItems.Count);

                System.IO.StreamWriter writer = new System.IO.StreamWriter(Environment.CurrentDirectory
                    + "/GeneratedRom.mem");

                foreach(var line in text)
                    writer.WriteLine(String.Join(" ",line.ToArray()));
                writer.Close();
            }
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData) {
            if (keyData == (Keys.Enter)) {
                button1.PerformClick();
                return true;
            } else if (keyData == (Keys.Control | Keys.S) ){
                buttonSaveToFile.PerformClick();
                return true;
            } else if (keyData == Keys.Escape) {
                reset(); 
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }
        private void reset() {
            textBox1.Text = textBox2.Text = textBoxCurrentAddress.Text = "";
            listBox1.SelectedValue = "0000";
            listBox2.SelectedValue = "000";
            listBox3.SelectedValue = listBox4.SelectedValue = "00";
            listBox5.SelectedValue = "00000";
            listBox6.SelectedValue = "00";
            listBox7.SelectedValue = listBox8.SelectedValue = listBox9.SelectedValue = listBox10.SelectedValue = listBox11.SelectedValue = "0";
            textBoxCurrentAddress.Focus();
        }
    }
}
