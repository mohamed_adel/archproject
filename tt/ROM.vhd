
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
Entity Rom is
 port (
address : in std_logic_vector(11 downto 0);
dataout : out std_logic_vector(34 downto 0) );
end entity Rom;


architecture syncrama of Rom is  
     type ROM_type is array(0 to 4095) of std_logic_vector(34 downto 0);
signal ram : ROM_type ;
Begin
dataout <= ram(to_integer(unsigned((address)))); 
end architecture syncrama;

