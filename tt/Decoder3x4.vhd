
library ieee;
Use ieee.std_logic_1164.all;
Entity Decoder3x8 is
port( 
      S:in std_logic_vector(2 downto 0);
      enable: in std_logic;
      outs:out std_logic_vector(7 downto 0));
end Decoder3x8;

Architecture Decoder3x8 of Decoder3x8 is
Component Decoder2X4 is
port( 
      S:in std_logic_vector(1 downto 0);
      enable: in std_logic;
      outs:out std_logic_vector(3 downto 0));
end component;
signal E1,E2:std_logic;
begin
E1<=(enable and (not S(2) ));
E2<=(enable and ( S(2) ));
decoder1: Decoder2X4 port map(S(1 downto 0),E1,outs(3 downto 0));
decoder2: Decoder2X4 port map(S(1 downto 0),E2,outs(7 downto 4));
end Decoder3x8;