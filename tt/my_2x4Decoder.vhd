Library ieee;
use ieee.std_logic_1164.all;

 -- Decoder2To4_Behavioral.vhd
  entity Decoder2to4 is
      port (A,B:  in std_logic;
              O1:  out std_logic_vector(3 downto 0));
  end Decoder2To4;
  
  architecture DecIns of Decoder2To4 is
  begin
     O1(3) <= (not A) and (not B);
     O1(2) <= (not A) and B;
     O1(1) <= A and (not B);
     O1(0) <= A and B;
 end DecIns;
