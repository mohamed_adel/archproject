library ieee;
use ieee.std_logic_1164.all;
Entity tagreb is
port(
clk :in std_logic);
end tagreb;
architecture tagreb  of tagreb is 
signal s:std_logic :='1';
begin
process(clk) is
begin
if rising_edge(clk) then 
s<='0';
end if;
end process ;
end tagreb;