Library ieee;
Use ieee.std_logic_1164.all;

-- interface 
entity ALSU is
port( s4,s3,s2,s1,s0,cin : in std_logic;
           A,B : in std_logic_vector (15 downto 0);
		   cout,zeroF,overflowF,negativeF : out std_logic;
           f : out  std_logic_vector (15 downto 0));
end ALSU;


-- implementation
architecture struct of ALSU is
-- includes 
	component Arithmetic is 
		generic (n : integer := 8);
		port ( s0,s1,cin : in std_logic ;
			A,B  : in std_logic_vector (n-1 downto 0);
			cout,overflowF : out std_logic ;
			f    : out std_logic_vector (n-1 downto 0) );
	end component;
	
	component Logic	is	
		generic (n : integer := 8);
		port ( s0,s1 : in std_logic ;
	    A,B : in std_logic_vector (n-1 downto 0);  
		f   : out std_logic_vector (n-1 downto 0) );
	end component;
	
    component Right  is  
		generic (n : integer := 8);
		port ( s0,s1,cin : in std_logic ;
	    A : in std_logic_vector (n-1 downto 0); 
		cout : out std_logic;
		f : out std_logic_vector (n-1 downto 0) );
	end component ;
	
	component Left  is  
		generic (n : integer := 8);
		port ( s0,s1,cin : in std_logic ;
	    A : in std_logic_vector (n-1 downto 0); 
		cout : out std_logic;		
		f : out std_logic_vector (n-1 downto 0) );
	end component ;
	
	component Variant is 
		generic (n : integer := 8);
		port ( s0,s1,cin : in std_logic ;
			A,B  : in std_logic_vector (n-1 downto 0);
			cout,overflowF : out std_logic ;
			f    : out std_logic_vector (n-1 downto 0) );
	end component;

-- wires 
	   signal  F1,F2,F3,F4,F5,A1,B1,zero,ff : std_logic_vector (15 downto 0);
	   signal  c1,c2,c3,c4,c5,of1,of2 : std_logic;

-- inserting actual modules
-- concurrancy 
   begin          
	zero <= (15 downto 0 => '0');   

  u0 : Arithmetic generic map (16) port map (s0,s1,cin,A,B,c1,of1,F1);
  u1 : Logic generic map (16) port map (s0,s1,A,B,F2) ;
  u2 : Right generic map (16) port map (s0,s1,cin,A,c3,F3);  
  u3 : Left generic map (16) port map (s0,s1,cin,A,c4,F4);
  u4 : Variant generic map (16) port map (s0,s1,cin,A,B,c5,of2,F5);
  
  
    ff <=  F1 when (s3 = '0' and s2 = '0' and s4='0')     
		ELSE F2 when (s3 = '0' and s2 = '1' and s4='0')
		ELSE F3 when (s3 = '1' and s2 = '0' and s4='0')     
		ELSE F4 when (s3 = '1' and s2 = '1' and s4='0')
		else F5;
	
	cout <=  c1 when (s3 = '0' and s2 = '0' and s4='0')     
		ELSE '0' when (s3 = '0' and s2 = '1' and s4='0')     
		ELSE c3 when (s3 = '1' and s2 = '0' and s4='0')     
		ELSE c4 when (s3 = '1' and s2 = '1' and s4='0')
		ELSE c5;
		
	overflowF <= of1 when s4='0'
	else of2;
		
	process(s0,s1,s2,s3,s4,ff)
	begin
		if not(s0='1' and s1='0' and s2='1' and s3='0' and s4='0') or not(s0='1' and s1='0' and s4='1') then
			f <= ff;
		end if;
	end process;
	
	zeroF <= '1' when (ff=zero)
	else '0';
	negativeF <= ff(15);
    
end struct;

