library ieee ;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
entity ControlUnit is

port(	IR:	in std_logic_vector(15 downto 0);
	Flags:	in std_logic_vector(15 downto 0);
	ControlWord:	out std_logic_vector(22 downto 0);
	Rest :in std_logic;
	OUT_ADDRESS :out std_logic_vector(11 downto 0);
	CLK:in std_logic);
end ControlUnit;
architecture ControlUnit of ControlUnit is 
Component MicroInstructionDecoder is
port(	IR:	in std_logic_vector(15 downto 0);
	Flags:	in std_logic_vector(15 downto 0);
	NextAdress:in std_logic_vector(11 downto 0);
	PLAout: in std_logic;
	Output :out std_logic_vector(11 downto 0));
end component;
Component nMasterSlave is
Generic ( n : integer := 16);
port( Clk,Rest : in std_logic;
D : in std_logic_vector(n-1 downto 0);
Q,notQ : out std_logic_vector(n-1 downto 0));
end Component;
Component Rom is
 port (
address : in std_logic_vector(11 downto 0);
dataout : out std_logic_vector(34 downto 0) );
end component ;
signal Rom_output,RomTemp :std_logic_vector(34 downto 0) :="00000000000000000000000000000000000";
signal Address :std_logic_vector(11 downto 0) :="000000000000";
signal NotAdress :std_logic_vector(11 downto 0) :="111111111111";
signal DecoderOutPut :std_logic_vector(11 downto 0) :="000000000000";
begin 
lblMAR: nMasterSlave generic map (12)  port map(CLK,Rest,DecoderOutPut,Address,NotAdress);
Decoder: MicroInstructionDecoder port map(IR,Flags,Rom_output(34 downto 23),Rom_output(0),DecoderOutPut);
RomMemory: Rom port map(Address,RomTemp);
ControlWord<=Rom_output(22 downto 0);
Rom_output<=RomTemp when Rest='0'
else "00000000000000000000000000000000000" when Rest='1';
OUT_ADDRESS<=Address;
end architecture;