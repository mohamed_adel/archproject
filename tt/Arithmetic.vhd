Library ieee;
Use ieee.std_logic_1164.all;
entity Arithmetic is 
	generic (n : integer := 8);
	port ( s0,s1,cin : in std_logic ;
	    A,B  : in std_logic_vector (n-1 downto 0);
		cout,overflowF : out std_logic ;
		f    : out std_logic_vector (n-1 downto 0) );
end entity Arithmetic;


-- take care of the usage of when else 
architecture  Data_flow of Arithmetic is
	Component my_nadder is
		generic (n : integer := 8);
		port (
			a, b : in std_logic_vector(n-1 downto 0) ;
			cin : in std_logic;
			s : out std_logic_vector(n-1 downto 0);
			cout,overflowF : out std_logic);
	end Component;
	signal bb,ff,zero,notOne: std_logic_vector(15 downto 0) ;
	signal c,co : std_logic;
	
begin
	zero <= (n-1 downto 0 => '0');
	notOne <= (n-1 downto 1 => '1') &'0';
   process(B,s0,s1,cin)
	begin
		if(s1 = '0' and s0 ='0') then
			bb <= zero;
			c  <= cin;
		elsif (s1 = '0' and s0 ='1') then
			bb <= B;
			c  <= cin;
		elsif (s1 = '1' and s0 ='0') then
			bb <= not B;
			c  <= cin;
		else
			bb <= notOne;
			c  <= '1';
		end if;
	end process;
	u0: my_nadder generic map (16) port map(A,bb,c,ff,co,overflowF);
	
	f <= zero when (s1 = '1' and s0 ='1' and cin = '1') 
	else ff;
	
	cout <= '0' when (s1 = '1' and s0 ='1' and cin = '1') 
	else co;
	
end Data_flow;
