Library ieee;
Use ieee.std_logic_1164.all;
entity Arithmetic is 
	generic (n : integer := 8);
	port ( 
		s0,s1,cin : in std_logic ;
	    A,B  : in std_logic_vector (n-1 downto 0);
		cout,overflowF : out std_logic ;
		f    : out std_logic_vector (n-1 downto 0) 
	);
end entity Arithmetic;


-- take care of the usage of when else 
architecture  Arithmetic_a of Arithmetic is
	Component my_nadder is
		generic (n : integer := 8);
		port (
			a, b : in std_logic_vector(n-1 downto 0) ;
			cin : in std_logic;
			s : out std_logic_vector(n-1 downto 0);
			cout,overflowF : out std_logic
		);
	end Component;
	signal bb,ff,zero,notOne: std_logic_vector(n-1 downto 0) ;
	signal c,co : std_logic;
	
begin
	zero <= (n-1 downto 0 => '0');
	notOne <= (n-1 downto 1 => '1') &'0';
	
	bb <= zero when (s1 = '0' and s0 ='0')
	else B when (s1 = '0' and s0 ='1')
	else not B when (s1 = '1' and s0 ='0')
	else notOne;
	
	c <= '1' when (s1 xor s0) = '0'
	else cin when (s1 = '0' and s0 ='1')
	else not cin;
	
	u0: my_nadder generic map (n) port map(A,bb,c,f,cout,overflowF);
	
end Arithmetic_a;
