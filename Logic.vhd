Library ieee;
Use ieee.std_logic_1164.all;

entity Logic is 
	generic (n : integer := 8);
	port ( 
		s0,s1 : in std_logic ;
	    A,B : in std_logic_vector (n-1 downto 0);  
		f   : out std_logic_vector (n-1 downto 0)
	);
end entity Logic;

-- take care of the usage of when else 
architecture  Data_flow of Logic is
begin
	
    f <=   A and (not B) WHEN s1 = '0' and s0 ='0'
       ELSE A and B WHEN s1 = '0' and s0 ='1'
	   ELSE A or B WHEN s1 = '1' and s0 ='0'
	   ELSE A xor B;
	
end Data_flow;
