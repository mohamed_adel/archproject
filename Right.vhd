Library ieee;
Use ieee.std_logic_1164.all;

entity Right is 
	generic (n : integer := 8);
	port ( 
		s0,s1,cin : in std_logic ;
		A : in  std_logic_vector (n-1 downto 0);
		cout,overflowF : out std_logic;
		f : out std_logic_vector (n-1 downto 0)
	);
end entity Right;

architecture  Data_flow of Right is
begin

	f <=	'0' & A(n-1 downto 1) when s1='0' and  s0='0'
			else A(0) & A(n-1 downto 1) when s1='0' and  s0='1'
			else cin	   & A(n-1 downto 1) when s1='1' and  s0='0'
			else A(n-1) & A(n-1 downto 1) when s1='1' and  s0='1';
	
	cout <= A(0);
	
	overflowF <=	A(n-1) when s1='0' and  s0='0'
			else A(0) xor A(n-1) when s1='0' and  s0='1'
			else cin xor A(n-1) when s1='1' and  s0='0'
			else '0' when s1='1' and  s0='1';


end Data_flow;

	     