Library ieee;
use ieee.std_logic_1164.all;

entity Decoder3To8 is
      port( S2,S1,S0:    in std_logic;
              O:      out std_logic_vector(7 downto 0));
  end Decoder3To8;
  
  architecture DecIns of Decoder3To8 is
 component Decoder2to4
     port (A, B:   in std_logic;
             O1:   out std_logic_vector(3 downto 0));
 end component;
 signal X1: std_logic_vector(3 downto 0);
  
 
 begin
 Dec2To4_1:
     Decoder2To4 port map( S1,  S0,  X1 );
 
     O(7) <= X1(0) and S2;
     O(6) <= X1(1) and S2;
     O(5) <= X1(2) and S2;
     O(4) <= X1(3) and S2;
 
     O(3) <= X1(0) and (not S2);
     O(2) <= X1(1) and (not S2);
     O(1) <= X1(2) and (not S2);
     O(0) <= X1(3) and (not S2);
 end DecIns;