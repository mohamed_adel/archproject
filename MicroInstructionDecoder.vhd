library ieee ;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
entity MicroInstructionDecoder is

port(	IR:	in std_logic_vector(15 downto 0);
	Flags:	in std_logic_vector(15 downto 0);
	NextAdress:in std_logic_vector(11 downto 0);
	PLAout: in std_logic;
	Output :out std_logic_vector(11 downto 0)
);
end MicroInstructionDecoder;

Architecture MicroInstructionDecoder of MicroInstructionDecoder is 
signal TwoWideBranchOperand : std_logic_vector(11 downto 0);
signal OneWideBranchOperand : std_logic_vector(11 downto 0);
signal  PLAoutput: std_logic_vector(11 downto 0);
begin
-- this Component to hanlde UAR branching and drive it to the right path

-- bracnh to start fetching src (100 110 120 140 160)
TwoWideBranchOperand  <= ("000001000000" or "000001" & IR(9 downto 7) & "000" ) when (NextAdress = "000000000100") and IR(9 downto 8)="00"
else ("000001000000" or "000001" & IR(9 downto 8) & "0000" ) when (NextAdress = "000000000100") and IR(9 downto 8)/="00"
-- branch to 166 or 170 depend on Indirect bit    
else ("000001111000") when NextAdress = "000001110110" and IR(7)='0'
else ("000001110110") when NextAdress = "000001110110" and IR(7)='1'
-- branch to start fetching dest (200 210 220 240 260) when next address = 200 Octal  
else ("000010000000" or "000010" & IR(4 downto 2) & "000" ) when NextAdress = "000010000000" and IR(4 downto 2)="00"     
else ("000010000000" or "000010" & IR(4 downto 3) & "0000" ) when NextAdress = "000010000000" and IR(4 downto 2)/="00" 
-- branch to 266 or 267 depend on Indirect bit when next= 266
else ("000010111000") when NextAdress = "000010110110" and IR(7)='0'
else ("000010110110") when NextAdress = "000010110110" and IR(7)='1'
-- branch to operation place when next instruction equall to 300 "ocatal" just encode value 
-- add operation 
									   
else ("000010111000") when (IR(15 downto 10) = "000001") and NextAdress = "000011000000" 
--sub operation
else ("000010111000") when (IR(15 downto 10) = "000010") and NextAdress = "000011000000"
--bic operation
else ("000010111000") when (IR(15 downto 10) = "000100") and NextAdress = "000011000000"
--bit operation
else ("000010111000") when (IR(15 downto 10) = "001100") and NextAdress = "000011000000"
--bis operation
else ("000010111000") when (IR(15 downto 10) = "000101") and NextAdress = "000011000000"
--xor operation
else ("000010111000") when (IR(15 downto 10) = "000110") and NextAdress = "000011000000"
--cmp operation
else ("000010111000") when (IR(15 downto 10) = "000111") and NextAdress = "000011000000"
-- branch to 204 or 205 depend on dest is register direct or not
else ("00001000010" & (IR(4) or IR(3) or IR(2)) ) when NextAdress = "000010000100";
-- END TWO OPERAND DECODER --

-- START ONE OPERAND DECODER --

-- bracnh to start fetching dest (200 210 220 240 260)
OneWideBranchOperand  <= ("000010000000" or "000010" & IR(4 downto 2) & "000" ) when NextAdress = "000000000100" and IR(4 downto 3)="00"
else ("000010000000" or "000010" & IR(4 downto 3) & "0000" ) when NextAdress = "000000000100" and IR(4 downto 3)/="00"

-- Inc operation 
else ("000010111000") when (IR(15 downto 5) = "10000000000") and NextAdress = "000011000000" 
-- Dec operation 
else ("000010111000") when (IR(15 downto 5) = "10000000011") and NextAdress = "000011000000"
-- Clr operation 
else ("000010111000") when (IR(15 downto 5) = "10000010011") and NextAdress = "000011000000" 
-- Inv operation 
else ("000010111000") when (IR(15 downto 5) = "10000000111") and NextAdress = "000011000000" 
-- LSR operation 
else ("000010111000") when (IR(15 downto 5) = "10000001000") and NextAdress = "000011000000" 
-- ROR operation 
else ("000010111000") when (IR(15 downto 5) = "10000001001") and NextAdress = "000011000000" 
-- RORC operation 
else ("000010111000") when (IR(15 downto 5) = "10000001010") and NextAdress = "000011000000" 
-- ASR operation 
else ("000010111000") when (IR(15 downto 5) = "10000001011") and NextAdress = "000011000000" 
-- LSL operation 
else ("000010111000") when (IR(15 downto 5) = "10000001100") and NextAdress = "000011000000" 
-- ROL operation 
else ("000010111000") when (IR(15 downto 5) = "10000001101") and NextAdress = "000011000000"
-- ROLC operation 
else ("000010111000") when (IR(15 downto 5) = "10000001110") and NextAdress = "000011000000"
-- ASL operation 
else ("000010111000") when (IR(15 downto 5) = "10000001111") and NextAdress = "000011000000"
-- JMP operation 
else ("000010111000") when (IR(15 downto 5) = "10000100000") and NextAdress = "000011000000"
-- branch to 204 or 205 depend on dest is register direct or not
else ("00001000010" & (IR(4) or IR(3) or IR(2)) ) when NextAdress = "000010000100";

-- END ONE OPERAND DECODER --

-- PLA section Section --
PLAoutput<=TwoWideBranchOperand when ((IR(15 downto 14) = "00" and IR(15 downto 10) /= "000000"))
else OneWideBranchOperand when (IR(15 downto 14) = "01");


-- Main Section --
Output <=PLAoutput when PLAout='1'
else NextAdress when PLAout='0';
end MicroInstructionDecoder;
