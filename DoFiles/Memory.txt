vsim work.main
add wave sim:/main/*
mem load -filltype inc -filldata 100 -fillradix hexadecimal -skip 0 /main/RamMemory/ram


force -freeze CLK 1 0, 0 {50 ns} -r 100
force rest 1
force -freeze InternalBus 16'h0000 0
force Rout "00" 
force Rin "00"
force MARin 0
force MARout 0
force MDRin 0
force MDRout 0
force rd 0
force rw 0
force mrw 0
force mrd 0
run
force rest 0
run 



force -freeze sim:/main/InternalBus 16'h8888 0
force Rin 1
force rd 1
run

force -freeze sim:/main/InternalBus 16'h9999 0
force Rin 0
run
force rd 0

force -freeze sim:/main/InternalBus 16'h0004 0
force MARin 1
force rd 1
run 
force rd 0
force -freeze mrw 0
force -freeze mrd 1
run


noforce sim:/main/InternalBus

force -freeze mrd 0
force MDRout 1
force Rin 0
force rd 1
run
force MDRout 0
force rd 0


force Rout 1
force MDRin 1
force rw 1
run 

force MDRin 0
force rw 0
force -freeze mrw 1
run 
force -freeze mrw 0

force -freeze sim:/main/InternalBus 16'h0001 0
force MARin 1
run
noforce sim:/main/InternalBus
force -freeze mrd 1
run
force -freeze mrd 0

force -freeze sim:/main/InternalBus 16'h0004 0
force MARin 1
run
noforce sim:/main/InternalBus
force -freeze mrd 1
run
force -freeze mrd 0
force rd 0
run





