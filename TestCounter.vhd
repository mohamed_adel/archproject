LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_unsigned.all;

ENTITY test_tb IS 
END test_tb;

ARCHITECTURE behavior OF test_tb IS
COMPONENT Counter is

generic(n: natural :=2);
port(	CLK:	in std_logic;
	clear:	in std_logic;
	Output:	out std_logic_vector(n-1 downto 0)
);
end COMPONENT;
   signal clk : std_logic := '0';
   signal reset : std_logic := '0';
   signal count : std_logic_vector(3 downto 0);
   constant clk_period : time := 1 ns;
BEGIN
   uut: Counter generic map (4) PORT MAP (
         clk => clk,
          Output => count,
          clear => reset
        );
   clk_process :process
   begin
        clk <= '0';
        wait for clk_period/2; 
        clk <= '1';
        wait for clk_period/2; 
   end process;
  stim_proc: process
   begin         
        wait for 7 ns;
        reset <='1';
        wait for 3 ns;
        reset <='0';
        wait for 17 ns;
        reset <= '1';
        wait for 1 ns;
        reset <= '0';
        wait;
  end process;

END;
