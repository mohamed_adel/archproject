Library ieee;
Use ieee.std_logic_1164.all;
entity Variant is 
	generic (n : integer := 8);
	port ( s0,s1,cin : in std_logic ;
	    A,B  : in std_logic_vector (n-1 downto 0);
		cout,overflowF : out std_logic ;
		f    : out std_logic_vector (n-1 downto 0) );
end entity Variant;


-- take care of the usage of when else 
architecture  Data_flow of Variant is
	Component my_nadder is
		generic (n : integer := 8);
		port (a, b : in std_logic_vector(n-1 downto 0) ;
				cin : in std_logic;
				s : out std_logic_vector(n-1 downto 0);
				cout,overflowF : out std_logic);
	end Component;
	signal bb,ff,zero,notOne: std_logic_vector(15 downto 0) ;
	signal c,co : std_logic;
	
begin
	zero <= (n-1 downto 0 => '0');
	notOne <= (n-1 downto 1 => '1') &'0';
	
	bb <= not B;
	c <= '0';
	
	u0: my_nadder generic map (n) port map(A,bb,c,ff,co,overflowF);
	
	f <= B when s1='0' and s0='0'
	else ff when s1='0' and s0='1'
	else zero when s1='1' and s0='0'
	else not A;
	
	process(s0,s1,co)
	begin
		if s1='0' and s0='1' then
			cout <= co;
		else
			overflowF <= '0';
		end if;
	end process;
	
end Data_flow;
