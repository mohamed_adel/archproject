library ieee;
Use ieee.std_logic_1164.all;
entity Decoder2X4 is 
port( 
      S:in std_logic_vector(1 downto 0);
      enable: in std_logic;
      outs:out std_logic_vector(3 downto 0));
end Decoder2X4;
architecture Decoder2X4 of Decoder2X4 is 
begin 
outs(0)<=((not S(1))and (not S(0))) and enable;
outs(1)<=((not S(1))and ( S(0))) and enable;
outs(2)<=(( S(1))and (not S(0))) and enable ;
outs(3)<=(( S(1))and ( S(0))) and enable;
end architecture;