
library ieee;
use ieee.std_logic_1164.all;
Entity testbench is
end testbench;

Architecture testbench_a of testbench is
component main is
port( 
Rout,Rin: in std_logic_vector(2 downto 0);
MARin,MARout,MDRin,MDRout: in std_logic;
rd,rw : in std_logic;  -- read write signals for registers
rest : in std_logic; -- rest signal 
mrw,mrd : in std_logic; -- memory read / write signal 
CLK : in std_logic;
InternalBus : inout std_logic_vector(15 downto 0));
end component;
signal tRout,tRin: std_logic_vector(2 downto 0);
signal tMARin,tMARout,tMDRin,tMDRout: std_logic;
signal trd,trw : std_logic;  -- read write signals for registers
signal trest : std_logic; -- rest signal 
signal tmrw,tmrd : std_logic; -- memory read / write signal 
signal tCLK : std_logic :='0';
signal tInternalBus : std_logic_vector(15 downto 0);
begin
process
begin
tCLK <= not tCLK;
wait for 2 ns;
end process;
process
begin
trest<='1';
tRout<='0';
tRin<='0';
tMARin<='0';
tMARout<='0';
tMDRin<='0';
tMDRout<='0';
trd<='0';
trw<='0';
tmrw<='0';
tmrd<='0';
tInternalBus <= "0000000000000000";
wait for 4 ns;
trest<='0';
wait for 4 ns;
tInternalBus <= "1000100010001000";
tRin <= "000";
trd <='1';
wait for 4 ns;
trd <='0';
wait for 4 ns;
tInternalBus <= "0000000000000000";
wait for 4 ns;
tRout <= "000";
trw <='1';
tMDRin<='1';
wait for 4 ns;
trw <='0';
tMDRin<='0';
tInternalBus <= "0000000000000001";
tMARin<='1';
tmrd<='1';
wait for 4 ns;
tmrd<='0';

tInternalBus <= "0000000000000000";
tMARin<='1';
wait for 4 ns;

tInternalBus <= "0000000000000001";
tMARin<='1';
tmrd<='1';
wait for 4 ns;
tmrd<='0';
tMARin<='0';
tMDRout<='1';
tRin<="001";
trd<='1';
wait for 4 ns;

tMDRout<='0';
tRin<="001";
trd<='0';
trw<='1';
wait for 4 ns;
assert(tInternalBus = "1000100010001000")
report  "interntal bus not ini with right value"
severity error;
end process;
process
begin
wait for  100 ns;
end process;
uut: main port map (tRout,tRin,tMARin,tMARout,tMDRin,tMDRout,trd,trw,trest,tmrw,tmrd,tCLK,tInternalBus);
end testbench_a;