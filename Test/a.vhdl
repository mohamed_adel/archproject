library ieee ;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
entity MicroInstructionDecoder is

port(	IR:	in std_logic_vector(15 downto 0);
	Flags:	in std_logic_vector(15 downto 0);
	NextAdress:in std_logic_vector(11 downto 0);
	PLAout: in std_logic;
	Output :out std_logic_vector(11 downto 0)
);
end MicroInstructionDecoder;

Architecture MicroInstructionDecoder of MicroInstructionDecoder is 
signal TwoWideBranchOperand : std_logic_vector(11 downto 0);
signal OneWideBranchOperand : std_logic_vector(11 downto 0);
signal  PLAoutput: std_logic_vector(11 downto 0);
begin
-- this Component to hanlde UAR branching and drive it to the right path

-- bracnh to start fetching src (100 110 120 140 160)
TwoWideBranchOperand  <= ("000001000000" or "000001" & IR(9 downto 7) & "000" ) when (NextAdress = "000000000011")
-- branch to 166 or 167 depend on Indirect bit 
else ("00000111011" & NOT IR(7)) when NextAdress = "000001110110"
-- branch to start fetching dest (200 210 220 240 260) when next address = 200 Octal  
else ("000010000000" or "000010" & IR(4 downto 2) & "000" ) when NextAdress = "000010000000"     
-- branch to 266 or 267 depend on Indirect bit 
else ("00001011011" & NOT IR(7)) when NextAdress = "000010110110"
-- branch to operation place when next instruction equall to 270 "ocatal"
-- add operation 
else ("000010111000") when (IR(15 downto 10) = "000001") and NextAdress = "000010111000" 
--sub operation
else ("000010111000") when (IR(15 downto 10) = "000010") and NextAdress = "000010111000"
--bic operation
else ("000010111000") when (IR(15 downto 10) = "000100") and NextAdress = "000010111000"
--bit operation
else ("000010111000") when (IR(15 downto 10) = "001100") and NextAdress = "000010111000"
--bis operation
else ("000010111000") when (IR(15 downto 10) = "000101") and NextAdress = "000010111000"
--xor operation
else ("000010111000") when (IR(15 downto 10) = "000110") and NextAdress = "000010111000"
--cmp operation
else ("000010111000") when (IR(15 downto 10) = "000111") and NextAdress = "000010111000"
-- branch to 204 or 205 depend on dest is register direct or not
else ("00001000010" & (IR(4) or IR(3) or IR(2)) ) when NextAdress = "000010000100";
-- END TWO OPERAND DECODER --

-- START ONE OPERAND DECODER --

-- bracnh to start fetching dest (200 210 220 240 260)
OneWideBranchOperand  <= ("000010000000" or "000010" & IR(4 downto 2) & "000" ) when NextAdress = "000000000011"
-- Inc operation 
else ("000010111000") when (IR(15 downto 5) = "10000000000") and NextAdress = "000010111000" 
-- Dec operation 
else ("000010111000") when (IR(15 downto 5) = "10000000011") and NextAdress = "000010111000"
-- Clr operation 
else ("000010111000") when (IR(15 downto 5) = "10000010011") and NextAdress = "000010111000" 
-- Inv operation 
else ("000010111000") when (IR(15 downto 5) = "10000000111") and NextAdress = "000010111000" 
-- LSR operation 
else ("000010111000") when (IR(15 downto 5) = "10000001000") and NextAdress = "000010111000" 
-- ROR operation 
else ("000010111000") when (IR(15 downto 5) = "10000001001") and NextAdress = "000010111000" 
-- RORC operation 
else ("000010111000") when (IR(15 downto 5) = "10000001010") and NextAdress = "000010111000" 
-- ASR operation 
else ("000010111000") when (IR(15 downto 5) = "10000001011") and NextAdress = "000010111000" 
-- LSL operation 
else ("000010111000") when (IR(15 downto 5) = "10000001100") and NextAdress = "000010111000" 
-- ROL operation 
else ("000010111000") when (IR(15 downto 5) = "10000001101") and NextAdress = "000010111000"
-- ROLC operation 
else ("000010111000") when (IR(15 downto 5) = "10000001110") and NextAdress = "000010111000"
-- ASL operation 
else ("000010111000") when (IR(15 downto 5) = "10000001111") and NextAdress = "000010111000"
-- JMP operation 
else ("000010111000") when (IR(15 downto 5) = "10000100000") and NextAdress = "000010111000"
-- branch to 204 or 205 depend on dest is register direct or not
else ("00001000010" & (IR(4) or IR(3) or IR(2)) ) when NextAdress = "000010000100";

-- END ONE OPERAND DECODER --

-- PLA section Section --
PLAoutput<=TwoWideBranchOperand when ((IR(15 downto 14) = "00" and IR(15 downto 10) /= "000000"))
else OneWideBranchOperand when (IR(15 downto 14) = "01");


-- Main Section --
NextAdress <=PLAoutput when PLAout='1'
else PLAout='1' PLAout='0';
end MicroInstructionDecoder;