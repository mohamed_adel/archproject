Library ieee;
use ieee.std_logic_1164.all;
Entity ALL_BUSS is
port( 
  CLK,RESET :in std_logic;
  INTERNAL_BUSS :inout std_logic_vector(15 downto 0)
);
end ALL_BUSS;
--_______________________________________________________________________________________________________________
Architecture ARC_ALL_BUSS of ALL_BUSS is
  
Component my_nDFF is
	Generic ( n : integer := 16);
	port( Clk,Rst,enable : in std_logic;
	      d : in std_logic_vector(n-1 downto 0);
	      q : out std_logic_vector(n-1 downto 0));
end component;

Component Ntri is
 Generic ( n : integer := 16);
 port(ins : in std_logic_vector(n-1 downto 0);
      es: in std_logic;
      outs : out std_logic_vector(n-1 downto 0));
end component;

Component ram is
port(clk : in std_logic;
    we : in std_logic;
    address : in std_logic_vector(15 downto 0);
    datain : in std_logic_vector(15 downto 0);
    dataout : out std_logic_vector(15 downto 0) );
end component ;

Component ALSU is
port( s4,s3,s2,s1,s0,cin : in std_logic;
      A,B : in std_logic_vector (15 downto 0);
		  cout,zeroF,overflowF,negativeF : out std_logic;
      f : out  std_logic_vector (15 downto 0));
end Component;

Component ControlUnit is

port(	IR:	in std_logic_vector(15 downto 0);
	    Flags:	in std_logic_vector(15 downto 0);
	    ControlWord:	out std_logic_vector(22 downto 0);
	    Rest :in std_logic;
	    OUT_ADDRESS :out std_logic_vector(11 downto 0);
	    CLK:in std_logic);
end Component;

Component my_control_unit3 is 
  port( 
      IR : in std_logic_vector(15 downto 0); --Instruction register
      
      F1 : in std_logic_vector(3 downto 0);  --4 bits of 
      F2 : in std_logic_vector(2 downto 0);  --3 bits of 
      F3 : in std_logic_vector(1 downto 0);  --2 bits of 
      F4 : in std_logic_vector(1 downto 0);  --2 bits of 
  
      PC_In  : out std_logic;
      PC_Out : out std_logic;
      
      R0_In : out std_logic;
      R0_Out : out std_logic;
      R1_In : out std_logic;
      R1_Out : out std_logic;
      R2_In : out std_logic;
      R2_Out : out std_logic;
      R3_In : out std_logic;
      R3_Out : out std_logic;
      
      IR_In : out std_logic;
      MAR_In : out std_logic;
      MDR_In : out std_logic;
      MDR_Out : out std_logic;
      TMP_In : out std_logic;
      TMP_Out : out std_logic;
      SRC_In : out std_logic;
      SRC_Out : out std_logic;
      DST_In : out std_logic;
      DST_Out : out std_logic;
      Y_In : out std_logic;
      Z_In : out std_logic;
      Z_Out : out std_logic
    
      );
end component;

component CW2FS is
port(
	ControlWord:	in std_logic_vector(22 downto 0);
	F1:	out std_logic_vector(3 downto 0);
	F2:	out std_logic_vector(2 downto 0);
	F3:	out std_logic_vector(1 downto 0);
	F4:	out std_logic_vector(1 downto 0);
	F5:	out std_logic_vector(4 downto 0);
	F6:	out std_logic_vector(1 downto 0);
	F7:	out std_logic;
	F8:	out std_logic;
	F9:	out std_logic;
	F10:	out std_logic;
	F11:	out std_logic
);
  
end component ;
--_______________________________________________________________________________________________________________
signal  s_r0_out: std_logic_vector(15 downto 0);
signal  s_r1_out: std_logic_vector(15 downto 0);
signal  s_r2_out: std_logic_vector(15 downto 0);
signal  s_r3_out: std_logic_vector(15 downto 0);

signal  s_source_out: std_logic_vector(15 downto 0);
signal  s_dest_out: std_logic_vector(15 downto 0);
signal  s_ir_out: std_logic_vector(15 downto 0);
signal  s_temp_out: std_logic_vector(15 downto 0);

signal  s_mar_out: std_logic_vector(15 downto 0);
signal  s_mdr_out: std_logic_vector(15 downto 0);
signal  s_mdr_in: std_logic_vector(15 downto 0);
signal  s_mdr_enable: std_logic;
signal  s_ram_out: std_logic_vector(15 downto 0);

signal  s_not_clock: std_logic;

signal  s_tr_buss_mdr_in: std_logic_vector(15 downto 0);

--alsu signals 

signal s_carry_in: std_logic;
signal s_y_out :std_logic_vector(15 downto 0);
signal s_z_out :std_logic_vector(15 downto 0);
signal s_carry_out :std_logic;
signal s_zero_flag :std_logic;
signal s_overflow_flag :std_logic;
signal s_negarive_flag :std_logic;
signal s_alsu_out :std_logic_vector(15 downto 0);

--flag signals
signal s_flag_reg :std_logic_vector(15 downto 0);
--control unit signals
signal s_control_word :std_logic_vector(22 downto 0);

--caontains of control word
signal s_F0 : std_logic_vector(11 downto 0); --12 bits of the next address
signal s_F1 : std_logic_vector(3 downto 0);  --4 bits of 
signal s_F2 : std_logic_vector(2 downto 0);  --3 bits of
signal s_F3 : std_logic_vector(1 downto 0);  --2 bits of 
signal s_F4 : std_logic_vector(1 downto 0);  --2 bits of 
signal s_F5 : std_logic_vector(4 downto 0);  --5 bits of (ALU)
signal s_F6 : std_logic_vector(1 downto 0);  --2 bits of 
signal s_F7 : std_logic;                     --1 bits of 
signal s_F8 : std_logic;                     --1 bits of (carry) 
signal s_F9 : std_logic;                     --1 bits of 
signal s_F10: std_logic;                    --1 bits of enabling the change in flags
signal s_F11: std_logic;  


-- CONTROL SIGNALS 

  signal 
    s_PC_In ,s_PC_Out, 
    c_R0_IN , c_R0_OUT,
    c_R1_IN , c_R1_OUT,
    c_R2_IN , c_R2_OUT,
    c_R3_IN , c_R3_OUT,
    c_IR_IN , c_MAR_IN,
    c_MDR_IN ,c_MDR_OUT,
    c_TEMP_IN ,c_TEMP_OUT,
    c_SOURCE_IN ,c_SOURCE_OUT,
    c_DEST_IN ,c_DEST_OUT,
    c_Y_IN ,
    c_Z_IN , c_Z_OUT ,READ,WRITE: std_logic;
  --signals for r3 , pc
  signal r3_enable_in : std_logic;
  signal r3_enable_out : std_logic;
  -- signal to reset y register by general reset or signal y reset from f7
  signal s_y_reset : std_logic ;
-- _______________________________________________________________________________________________________________
begin
-- registers
R0: my_nDFF generic map (16)  port map(CLK,RESET,c_R0_IN,INTERNAL_BUSS,s_r0_out);-- general purpose register
R1: my_nDFF generic map (16)  port map(CLK,RESET,c_R1_IN,INTERNAL_BUSS,s_r1_out);-- general purpose register
R2: my_nDFF generic map (16)  port map(CLK,RESET,c_R2_IN,INTERNAL_BUSS,s_r2_out);-- general purpose register
R3: my_nDFF generic map (16)  port map(CLK,RESET,R3_enable_in,INTERNAL_BUSS,s_r3_out);-- general purpose register
    
SOURCE:my_nDFF generic map (16)  port map(CLK,RESET,c_SOURCE_IN,INTERNAL_BUSS,s_source_out);
DEST:  my_nDFF generic map (16)  port map(CLK,RESET,c_DEST_IN ,INTERNAL_BUSS,s_dest_out);
TEMP:  my_nDFF generic map (16)  port map(CLK,RESET,c_TEMP_IN ,INTERNAL_BUSS,s_temp_out);
IR:    my_nDFF generic map (16)  port map(CLK,RESET,c_IR_IN   ,INTERNAL_BUSS,s_ir_out);
 
-- alsu registers
Z:  my_nDFF generic map (16)  port map(CLK,RESET,c_Z_IN ,s_alsu_out ,s_z_out);
Y:  my_nDFF generic map (16)  port map(CLK,RESET,c_Y_IN ,INTERNAL_BUSS,s_y_out);  
 -- their tristates 
TR_R0: Ntri     generic map (16) port map(s_r0_out,c_R0_OUT,INTERNAL_BUSS);
TR_R1: Ntri     generic map (16) port map(s_r1_out,c_R1_OUT,INTERNAL_BUSS);
TR_R2: Ntri     generic map (16) port map(s_r2_out,c_R2_OUT,INTERNAL_BUSS);
TR_R3: Ntri     generic map (16) port map(s_r3_out,r3_enable_out,INTERNAL_BUSS);
  
TR_SOURCE: Ntri generic map (16) port map(s_source_out,c_SOURCE_OUT,INTERNAL_BUSS);
TR_DEST: Ntri   generic map (16) port map(s_dest_out,  c_DEST_OUT,  INTERNAL_BUSS);
TR_TEMP: Ntri   generic map (16) port map(s_temp_out,  c_TEMP_OUT,  INTERNAL_BUSS);

TR_TZ  : Ntri   generic map (16) port map(s_z_out,c_Z_OUT,INTERNAL_BUSS);
  
--MAR AND MDR ,RAM AND THEIR TRISTATES 
MAR: my_nDFF generic map (16)  port map(CLK,RESET,c_MAR_IN, INTERNAL_BUSS,s_mar_out);
MDR: my_nDFF generic map (16)  port map(CLK,RESET,s_mdr_enable,s_mdr_in,s_mdr_out);
  
TR_MDR_IN: Ntri generic map (16) port map(INTERNAL_BUSS,c_MDR_IN, s_tr_buss_mdr_in);
TR_MDR_OUT:Ntri generic map (16) port map(s_mdr_out,c_MDR_OUT,INTERNAL_BUSS);
  
s_not_clock<= not CLK;
RAM_MEM: ram port map(s_not_clock,WRITE,s_mar_out,s_mdr_out,s_ram_out);
  
s_mdr_enable<= c_MDR_IN  or (  not c_MDR_IN and READ); 

s_mdr_in<=s_tr_buss_mdr_in when c_MDR_IN='1'
else s_ram_out when c_MDR_IN='0' and READ='1';
  
--_________________________________________________________________________________________________________________________________
--flag register 
s_flag_reg<=s_zero_flag&s_overflow_flag&s_negarive_flag&s_carry_in&"000000000000"when s_F10='1'
else (15 downto 0 => '0');  
--_________________________________________________________________________________________________________________________________
-- arithmatic shift unit  20 functions 
ALSU_PROJ: ALSU port map (s_F5(4),s_F5(3),s_F5(2),s_F5(1),s_F5(0),s_carry_in,s_y_out,INTERNAL_BUSS,
                          s_carry_out,s_zero_flag,s_overflow_flag,s_negarive_flag,s_alsu_out);
--_________________________________________________________________________________________________________________________________
--control unit 
COUNTROL_UNIT_PROJ :ControlUnit port map(s_ir_out,s_flag_reg,s_control_word,RESET,s_F0,s_not_clock);
--__________________________________________________________________________________________________________________________________
--control signals
CONTROL_SIGNALS :my_control_unit3 port map(s_ir_out,s_F1,s_F2,s_F3,s_F4,
                                           s_PC_In,s_PC_Out,
                                           c_R0_IN , c_R0_OUT,
                                           c_R1_IN , c_R1_OUT,
                                           c_R2_IN , c_R2_OUT,
                                           c_R3_IN , c_R3_OUT,
                                           c_IR_IN ,c_MAR_IN,
                                           c_MDR_IN ,c_MDR_OUT,
                                           c_TEMP_IN ,c_TEMP_OUT,
                                           c_SOURCE_IN ,c_SOURCE_OUT,
                                           c_DEST_IN ,c_DEST_OUT,
                                           c_Y_IN ,
                                           c_Z_IN , c_Z_OUT);
--_____________________________________________________________________________________________________________________
-- convert control word to F
converter : CW2FS port map(s_control_word , s_F1 , s_F2 , s_F3 , s_F4 , s_F5 ,s_F6 , s_F7 , s_F8, s_F9 , s_F10,s_F11 );
READ<= s_F6(0) and not s_F6(1);
WRITE<=s_F6(1) and not s_F6(0);
s_y_reset <=RESET or s_F7;
s_carry_in <=s_F8;
--_____________________________________________________________________________________________________________________
--pc and r3 
r3_enable_in <=(s_PC_In or c_R3_IN);
r3_enable_out <=(s_PC_Out or c_R3_OUT);
end ARC_ALL_BUSS;


