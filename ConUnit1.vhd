Library ieee;
use ieee.std_logic_1164.all;

Entity my_control_unit3 is
  
port( 
      IR : in std_logic_vector(15 downto 0); --Instruction register
      
      F1 : in std_logic_vector(3 downto 0);  --4 bits of 
      F2 : in std_logic_vector(2 downto 0);  --3 bits of 
      F3 : in std_logic_vector(1 downto 0);  --2 bits of 
      F4 : in std_logic_vector(1 downto 0);  --2 bits of 
  
      PC_In  : out std_logic;
      PC_Out : out std_logic;
 
      R0_In : out std_logic;
      R0_Out : out std_logic;
      R1_In : out std_logic;
      R1_Out : out std_logic;
      R2_In : out std_logic;
      R2_Out : out std_logic;
      R3_In : out std_logic;
      R3_Out : out std_logic;
      
      IR_In : out std_logic;
      MAR_In : out std_logic;
      MDR_In : out std_logic;
      MDR_Out : out std_logic;
      TMP_In : out std_logic;
      TMP_Out : out std_logic;
      SRC_In : out std_logic;
      SRC_Out : out std_logic;
      DST_In : out std_logic;
      DST_Out : out std_logic;
      Y_In : out std_logic;
      Z_In : out std_logic;
      Z_Out : out std_logic
    
      );
end my_control_unit3;

Architecture a_my_control_unit3 of my_control_unit3 is
  
  component Decoder3To8 is
      port( S2,S1,S0:    in std_logic;
              O:      out std_logic_vector(7 downto 0));
  end component;
  
signal Rdst_Out , Rdst_In , Rsrc_Out , Rsrc_In : std_logic;
signal Out1 ,Out2 : std_logic_vector (7 downto 0);
signal PC_Out1 , MDR_Out1 ,Z_Out1 ,TMP_Out1 , SRC_Out1 ,DST_Out1,Rsrc_Out1,Rdst_Out1 : std_logic;
signal PC_In1  ,IR_In1  , Z_In1  , Rsrc_In1,Rdst_In1 : std_logic;
signal MAR_In1 , MDR_In1, TMP_In1 : std_logic;
signal Y_In1 ,SRC_In1,DST_In1 : std_logic;
  
begin
  
  PC_Out1   <=      F1(0)  and (not F1(1)) and (not F1(2)) and (not F1(3));--F1=0001 XXXX
  MDR_Out1  <= (not F1(0)) and      F1(1)  and (not F1(2)) and (not F1(3));--F1=0010
  Z_Out1    <=      F1(0)  and      F1(1)  and (not F1(2)) and (not F1(3));--F1=0011
  TMP_Out1  <= (not F1(0)) and      F1(1)  and (not F1(2)) and      F1(3); --F1=1010
  SRC_Out1  <= (not F1(0)) and (not F1(1)) and (not F1(2)) and      F1(3); --F1=1000
  DST_Out1  <=      F1(0)  and (not F1(1)) and (not F1(2)) and      F1(3); --F1=1001
  Rsrc_Out1 <= (not F1(0)) and (not F1(1)) and      F1(2)  and (not F1(3));--F1=0100
  Rdst_Out1 <=      F1(0)  and (not F1(1)) and      F1(2)  and (not F1(3));--F1=0101
  
      PC_Out   <= '0' when F1="0000"
    else PC_Out1;
      MDR_Out   <= '0' when F1="0000"
    else MDR_Out1;
      Z_Out   <= '0' when F1="0000"
    else Z_Out1;
      TMP_Out   <= '0' when F1="0000"
    else TMP_Out1;
      SRC_Out   <= '0' when F1="0000"
    else SRC_Out1;
      DST_Out   <= '0' when F1="0000"
    else DST_Out1;
      Rsrc_Out   <= '0' when F1="0000"
    else Rsrc_Out1;
      Rdst_Out   <= '0' when F1="0000"
    else Rdst_Out1;
      
  PC_In1   <=     F2(0)  and (not F2(1)) and (not F2(2));--F2=001
  IR_In1   <=(not F2(0)) and      F2(1)  and (not F2(2));--F2=010
  Z_In1    <=     F2(0)  and      F2(1)  and (not F2(2));--F2=011
  Rsrc_In1 <=(not F2(0)) and (not F2(1)) and      F2(2); --F2=100
  Rdst_In1 <=     F2(0)  and (not F2(1)) and      F2(2); --F2=101
  
      PC_In   <= '0' when F2="000"
    else PC_In1;
      IR_In   <= '0' when F2="000"
    else IR_In1;
      Z_In   <= '0' when F2="000"
    else Z_In1;
      Rsrc_In   <= '0' when F2="000"
    else Rsrc_In1;
      Rdst_In   <= '0' when F2="000"
	  else Rdst_In1;
      
  MAR_In1 <=      F3(0)  and (not F3(1));-- F3=01
  MDR_In1 <= (not F3(0)) and      F3(1); -- F3=10
  TMP_In1 <=      F3(0)  and      F3(1); -- F3=11
  
      MAR_In   <= '0' when F3="00"
    else MAR_In1;
      MDR_In   <= '0' when F3="00"
    else MDR_In1;
      TMP_In   <= '0' when F3="00"
	  else TMP_In1;
      
  Y_In1   <=      F4(0)  and (not F4(1));--F4=01
  SRC_In1 <= (not F4(0)) and      F4(1); --F4=10 
  DST_In1 <=      F4(0)  and      F4(1); --F4=11
  
     Y_In   <= '0' when F4="00"
    else Y_In1;
      SRC_In   <= '0' when F4="00"
    else SRC_In1;
      DST_In   <= '0' when F4="00"
	  else DST_In1;
  
      
      --The 2 Decoders
      D1 : Decoder3To8 port map( IR(1),IR(0),Rdst_Out,Out1);--Destenation decoder
      D2 : Decoder3To8 port map( IR(6),IR(5),Rsrc_Out,Out2);--source decoder
        
      R0_In  <= Out1(0) or Out2(0);
      R0_Out <= Out1(1) or Out2(1);
      R1_In  <= Out1(2) or Out2(2);
      R1_Out <= Out1(3) or Out2(3);
      R2_In  <= Out1(4) or Out2(4);
      R2_Out <= Out1(5) or Out2(5);
      R3_In  <= Out1(6) or Out2(6);
      R3_Out <= Out1(7) or Out2(7);

end a_my_control_unit3;