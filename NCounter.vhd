library ieee ;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
entity Counter is

generic(n: natural :=2);
port(	CLK:	in std_logic;
	clear:	in std_logic;
	Output:	out std_logic_vector(n-1 downto 0)
);
end Counter;

architecture behv of counter is		 	  
	
    signal temp: std_logic_vector(n-1 downto 0);

begin
    process(CLK, clear)
    begin
	if clear = '1' then
 	    temp <= temp - temp;
	elsif (falling_edge(CLK)) then
		temp <= temp + 1;
	end if;
    end process;	
    Output <= temp;

end behv;