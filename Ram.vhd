
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
Entity ram is
 port (clk : in std_logic;
           we : in std_logic;
address : in std_logic_vector(15 downto 0);
datain : in std_logic_vector(15 downto 0);
dataout : out std_logic_vector(15 downto 0) );
end entity ram;


architecture syncrama of ram is  
     type ram_type is array(0 to 65536) of std_logic_vector(15 downto 0);
signal ram : ram_type ;
Begin
process(clk) is  
Begin
    if rising_edge(clk) then   
	 if we = '1' then
	   ram(to_integer(unsigned((address))))<=datain;  
         end if;
   end if;
end process;
dataout <= ram(to_integer(unsigned((address)))); 
end architecture syncrama;
