Library ieee;
Use ieee.std_logic_1164.all;

entity Left is 
	generic (n : integer := 8);
	port ( 
		s0,s1,cin : in std_logic ;
	    A : in std_logic_vector (n-1 downto 0);  
		cout,overflowF : out std_logic;
		f : out std_logic_vector (n-1 downto 0)
	);	
end entity Left;

architecture  Data_flow of Left is
signal zero : std_logic_vector(n-1 downto 0) ;
begin
     zero <= (n-1 downto 0 => '0');
    
	f <= 	A(n-2 downto 0) & '0' 		when s0='0' and  s1='0'
		else	A(n-2 downto 0) & A(n-1)	when s0='0' and  s1='1'
		else	A(n-2 downto 0) & cin		when s0='1' and  s1='0'
		else	A(n-2 downto 0) & '0' when s0='1' and  s1='1';
		
	cout <= A(n-1);

	overflowF <= A(n-1) xor A(n-2);

end Data_flow;


