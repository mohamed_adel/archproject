library ieee;
use ieee.std_logic_1164.all;

Entity CW2FS is
port(
	ControlWord:	in std_logic_vector(22 downto 0);
	F1:	out std_logic_vector(3 downto 0);
	F2:	out std_logic_vector(2 downto 0);
	F3:	out std_logic_vector(1 downto 0);
	F4:	out std_logic_vector(1 downto 0);
	F5:	out std_logic_vector(4 downto 0);
	F6:	out std_logic_vector(1 downto 0);
	F7:	out std_logic;
	F8:	out std_logic;
	F9:	out std_logic;
	F10:	out std_logic;
	F11:	out std_logic
);
end CW2FS;
Architecture CW2FS of CW2FS is
begin 
	F1<=ControlWord(22 downto 19);
	F2<=ControlWord(18 downto 16);
	F3<=ControlWord(15 downto 14);
	F4<=ControlWord(13 downto 12);
	F5<=ControlWord(11 downto 7);
	F6<=ControlWord(6 downto 5);
	F7<=ControlWord(4 );
	F8<=ControlWord(3 );
	F9<=ControlWord(2 );
	F10<=ControlWord(1 );
	F11<=ControlWord(0 );
end CW2FS;