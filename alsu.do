vsim -gui work.alsu
add wave -position end  sim:/alsu/s4
add wave -position end  sim:/alsu/s3
add wave -position end  sim:/alsu/s2
add wave -position end  sim:/alsu/s1
add wave -position end  sim:/alsu/s0
add wave -position end  sim:/alsu/cin
add wave -position end  sim:/alsu/A
add wave -position end  sim:/alsu/B
add wave -position end  sim:/alsu/cout
add wave -position end  sim:/alsu/zeroF
add wave -position end  sim:/alsu/overflowF
add wave -position end  sim:/alsu/negativeF
add wave -position end  sim:/alsu/f
add wave -position end  sim:/alsu/ff
force -freeze sim:/alsu/A 16'hAAAA 0
force -freeze sim:/alsu/B 16'h1205 0
force -freeze sim:/alsu/cin 1 0
force -freeze sim:/alsu/s4 0 0, 1 {50 ns} -r 100
force -freeze sim:/alsu/s3 0 0, 1 {25 ns} -r 50
force -freeze sim:/alsu/s2 0 0, 1 {12 ns} -r 25
force -freeze sim:/alsu/s1 0 0, 1 {6 ns} -r 12.5
force -freeze sim:/alsu/s0 0 0, 1 {3 ns} -r 6
run

