Library ieee;
use ieee.std_logic_1164.all;
Entity main is
port( 
Rout,Rin: in std_logic_vector(2 downto 0);
MARin,MARout,MDRin,MDRout: in std_logic;
rd,rw : in std_logic;  -- read write signals for registers
rest : in std_logic; -- rest signal 
mrw,mrd : in std_logic; -- memory read / write signal 
CLK : in std_logic;
r0 : out std_logic_vector(15 downto 0);
InternalBus : inout std_logic_vector(15 downto 0));
end main;

Architecture main of main is
-------------------------------------- Component defination Start  --------------------------------------
Component Decoder3x8 is
               port( 
      		S:in std_logic_vector(2 downto 0);
      		enable: in std_logic;
      		outs:out std_logic_vector(7 downto 0));
end component;
Component my_nDFF is
	Generic ( n : integer := 16);
	port( Clk,Rst,enable : in std_logic;
	d : in std_logic_vector(n-1 downto 0);
	q : out std_logic_vector(n-1 downto 0));
end component;
Component Ntri is
Generic ( n : integer := 16);
port(
ins : in std_logic_vector(n-1 downto 0);
es: in std_logic;
outs : out std_logic_vector(n-1 downto 0));
end component;
Component ram is
 port (clk : in std_logic;
           we : in std_logic;
address : in std_logic_vector(15 downto 0);
datain : in std_logic_vector(15 downto 0);
dataout : out std_logic_vector(15 downto 0) );
end component ;

-- alu comp :

Component ALSU is
port( s4,s3,s2,s1,s0,cin : in std_logic;
           A,B : in std_logic_vector (15 downto 0);
		   cout,zeroF,overflowF,negativeF : out std_logic;
           f : out  std_logic_vector (15 downto 0));
end Component;

Component ControlUnit is

port(	IR:	in std_logic_vector(15 downto 0);
	Flags:	in std_logic_vector(15 downto 0);
	ControlWord:	out std_logic_vector(22 downto 0);
	Rest : std_logic;
	CLK:in std_logic);
end Component;


-------------------------------------- Component defination End --------------------------------------
signal  SrcOuts: std_logic_vector(7 downto 0);
signal  DestOuts: std_logic_vector(7 downto 0);

signal  RegisterTri0: std_logic_vector(15 downto 0); -- general purpose register signal
signal  RegisterTri1: std_logic_vector(15 downto 0); -- general purpose register signal
signal  RegisterTri2: std_logic_vector(15 downto 0); -- general purpose register signal
signal  RegisterTri3: std_logic_vector(15 downto 0); -- general purpose register signal

signal  RegisterTri4: std_logic_vector(15 downto 0); -- TEMP register signal
signal  RegisterTri5: std_logic_vector(15 downto 0); -- SRC register signal 
signal  RegisterTri6: std_logic_vector(15 downto 0); -- DEST register  signal
signal  RegisterTri7: std_logic_vector(15 downto 0); -- IR register signal 

signal  RegisterTri8: std_logic_vector(15 downto 0); -- Y register signal 
signal  RegisterTri9: std_logic_vector(15 downto 0); -- Z register signal 

signal  RegisterTri10: std_logic_vector(15 downto 0); -- FLAG register signal 

signal  RegisterMdrIn : std_logic_vector(15 downto 0);
signal  RegisterMDR : std_logic_vector(15 downto 0);
signal  RegisterMAR : std_logic_vector(15 downto 0);

signal  MemoryDatain : std_logic_vector(15 downto 0);
signal  MemoryDataout : std_logic_vector(15 downto 0);
signal NotClk : std_logic;
signal  MdrEnable : std_logic ;

---- FLAGS SIGNALS 
signal zeroFSignal,overflowFSignal,negativeFSignal : std_logic;
signal FlagRegisterInput : std_logic_vector(15 downto 0);
begin
NotClk <= not CLK;
RamMemory: ram port map(CLK,mrw,RegisterMAR,MemoryDatain,MemoryDataout);

source: Decoder3x8 port map(Rout,rw,SrcOuts);

destination: Decoder3x8 port map(Rin,rd,DestOuts);

Register0: my_nDFF generic map (16)  port map(CLK,rest,DestOuts(0),InternalBus,RegisterTri0);-- general purpose register
Register1: my_nDFF generic map (16)  port map(CLK,rest,DestOuts(1),InternalBus,RegisterTri1);-- general purpose register
Register2: my_nDFF generic map (16)  port map(CLK,rest,DestOuts(2),InternalBus,RegisterTri2);-- general purpose register
Register3: my_nDFF generic map (16)  port map(CLK,rest,DestOuts(3),InternalBus,RegisterTri3);-- general purpose register

Register4: my_nDFF generic map (16)  port map(CLK,rest,DestOuts(0),InternalBus,RegisterTri4);-- TEMP register signal
Register5: my_nDFF generic map (16)  port map(CLK,rest,DestOuts(1),InternalBus,RegisterTri5);-- SRC register signal 
Register6: my_nDFF generic map (16)  port map(CLK,rest,DestOuts(2),InternalBus,RegisterTri6);-- DEST register  signal
Register7: my_nDFF generic map (16)  port map(CLK,rest,DestOuts(3),InternalBus,RegisterTri7);-- IR register signal


r0<=RegisterTri1;
lblMdr: my_nDFF generic map (16)  port map(CLK,rest,MdrEnable,RegisterMdrIn,RegisterMDR);
lblMar: my_nDFF generic map (16)  port map(NotClk,rest,MARin,InternalBus,RegisterMAR);

RegisterMdrIn<= MemoryDataout when (mrd='1')
else InternalBus;
MdrEnable<=((mrd) or MDRin);

TriStateBuffer0: Ntri generic map (16) port map(RegisterTri0,SrcOuts(0),InternalBus);
TriStateBuffer1: Ntri generic map (16) port map(RegisterTri1,SrcOuts(1),InternalBus);
TriStateBuffer2: Ntri generic map (16) port map(RegisterTri2,SrcOuts(2),InternalBus);
TriStateBuffer3: Ntri generic map (16) port map(RegisterTri3,SrcOuts(3),InternalBus);

TriStateBuffer4: Ntri generic map (16) port map(RegisterTri4,SrcOuts(4),InternalBus);
TriStateBuffer5: Ntri generic map (16) port map(RegisterTri5,SrcOuts(5),InternalBus);
TriStateBuffer6: Ntri generic map (16) port map(RegisterTri6,SrcOuts(6),InternalBus);
TriStateBuffer7: Ntri generic map (16) port map(RegisterTri7,SrcOuts(7),InternalBus);

TriStateBufferMDRI: Ntri generic map (16) port map(RegisterMDR,MDRout,InternalBus);
TriStateBufferMDRE: Ntri generic map (16) port map(RegisterMDR,mrw,MemoryDatain);
TriStateBufferMAR: Ntri generic map (16) port map(RegisterMAR,MARout,InternalBus);

FlagRegisterInput<=zeroFSignal&overflowFSignal&negativeFSignal&"0000000000000";

AluLBL : ALSU port map();
ControlUnit : ControlUnit port map();
end main;

